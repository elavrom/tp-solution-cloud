#!/bin/sh
set -e

# Install dependencies, and execute composer tasks
composer install

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php-fpm "$@"
fi

exec "$@"