<?php

date_default_timezone_set('Europe/Paris');
include_once 'vendor/autoload.php';

class Minio
{
    private static $client;

    private static function getClient() {
        if (null === self::$client) {
            self::$client = new \Aws\S3\S3Client([
                'version' => 'latest',
                'region' => 'eu-west-3',
                'endpoint' => 'http://minio:9000',
                'use_path_style_endpoint' => true,
                'credentials' => [
                    'key' => getenv('MINIO_ACCESS_KEY'),
                    'secret' => getenv('MINIO_SECRET_KEY'),
                ]
            ]);

            if (empty(self::listBuckets()->get('Buckets'))) {
                self::createBucket();
            }
        }
        return self::$client;
    }

    public static function createBucket($bucket = 'gestion-produits-main')
    {
        return self::getClient()->createBucket([
            'Bucket' => $bucket
        ]);
    }

    public static function listBuckets()
    {
        return self::getClient()->listBuckets();
    }

    public static function putObject($key, $body, $bucket = 'gestion-produits-main')
    {
        return self::getClient()->putObject([
            'Bucket' => $bucket,
            'Key' => $key,
            'Body' => $body
        ]);
    }

    public static function getObject($key, $bucket = 'gestion-produits-main')
    {
        try {
            return self::getClient()->getObject([
                'Bucket' => $bucket,
                'Key' => $key
            ]);
        } catch (\Aws\S3\Exception\S3Exception $exception) {
            return [
                'NotFound' => true,
                'Body' => null
            ];
        }
    }

    public static function deleteObject($key, $bucket = 'gestion-produits-main')
    {
        return self::getClient()->deleteObject([
            'Bucket' => $bucket,
            'Key' => $key
        ]);
    }
}