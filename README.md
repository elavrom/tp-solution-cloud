# GESTION PRODUITS

## Prérequis
Cette application est compatible `PHP5` et a été testée avec une base de données `MySQL 5.7`.

## Installation
- Copier les fichiers du dossier `www` dans un dossier accessible par le serveur Web.
- Assurez vous que le dossier `uploads` est accessible en lecture et écriture par le serveur Web : `chmod 777 uploads`
- Importez la base de données test à partir du dump SQL `database/gestion_produits.sql`.
- Connectez vous à l'application avec l'url adaptée avec les informations suivantes :
    - Login : `admin`
    - Mot de passe : `password`
    
## Conteneurisation
- Il faut ajouter `127.0.0.1 romain.local` dans le fichier `/etc/hosts`
- Lancer les conteneurs avec `docker-compose up`

## Scalabilité
- Partant d'un fichier docker-compose, la scalabilité est simple à mettre en place. De petits ajouts ont été apportés (replicas par exemple). D'ailleurs, afin d'assurer une persistance de session, un serveur redis a été mis en place.
- Lancer `docker swarm init` pour lancer un swarm
- Lancer `docker stack deploy --compose-file=docker-compose.yml gestion-produits` pour déployer les tous les services inscrits dans le fichier docker-compose.yml.

##### Note
Sur ma machine, le service de base de données restait coincé en état "Preparing", rendant l'application inutilisable.
Cependant, la scalabilité horizontale fonctionne bien entre nginx et php (impossible de tester pour les autres services vu qu'il faut d'abord s'être connecté)


## Fonctionnalités
L'application permet de :
- Lister les produits
- Afficher la fiche produit en lecture seule
- Ajouter des produits
- Modifier les produits
- Supprimer les produits
- Pour chaque produit, il est possible d'ajouter autant de photos que nécessaire
